# rileytaylor.dev

> Personal site and blog

[![Netlify Status](https://api.netlify.com/api/v1/badges/a1f2a1a3-c704-4599-831c-9ade5fceacfb/deploy-status)](https://app.netlify.com/sites/rileytaylor/deploys)
[![Gitlab Pipeline](https://gitlab.com/rileytaylor/rileytaylor.dev/badges/master/pipeline.svg)](https://gitlab.com/rileytaylor/rileytaylor.dev/-/commits/master)
[![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)
[![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](todo)
[![MIT License](https://badgen.net/badge/license/MIT/blue)](https://badgen.net/badge/license/MIT/blue)

This project makes use of the following libraries and tools:

- [Nuxt.js](https://nuxtjs.org)
- [PostCSS](https://postcss.org)
- [Tailwind](https://tailwindcss.com/)
- [Storybook](https://storybook.js.org) via [@nuxt/storybook](https://storybook.nuxtjs.org/)

This is primarily a personal project, so I try to walk the line between following best-practices and ease of maintainability. Over time I hope to fill in some of the gaps here, such as testing, but in an effort to ensure I actually _have_ a site out in the world I'm picking and choosing what to implement for now.

## Getting started

Install:

```sh
npm install
cp .example.env .env #fill out env variables
npm run dev
```

That will serve the app at [](https://localhost:3000).

### Commands

- `npm run dev`: Run in development mode
- `npm run start`: Run in production mode
- `npm run generate`: Build as a static site
- `npm run build:storybook`: Build the storybook site
- `npm run lint`: Run linting. `lint:js` and `lint:css` also exist for those individual linting types.
- `npm run test`: Run jest testing

A few other commands exist as well for various nuxt deployment strategies such as SSR or webpack.

### Local Environment Configuration

The scoped `@rileytaylor` packages are hosted on Gitlab Package Registry and need a key to access. Add the following to `~/.npmrc`:

```bash
//gitlab.com/api/v4/packages/npm/:_authToken=<token>
```

where `<token>` is a gitlab api token with `api` access to `rileytaylor`.

### Test Posts

Several test posts exist to use during development of markdown content. Simply uncomment `TEST_POSTS="true"` in `.env` to use these instead of the actual posts.

### Images

Optimizing images is a really good idea before uploading to S3.

**Size:** If it's a blog post header image it should be 3:2 @ 1200x800px. Posts within blog content should ideally fit that too, though it is not necessary.

**Format:** JPG is generally the best for web universally at the moment. Long term, with the right setup, .webp with .jpg backup might be added.

Crop the image manually first, then run it through ImageMagick:

```sh
magick <input> -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB <output>
```

## Deployment

Netlify will prepare deploys for the `master` branch, which must be manually propogated in the netlify dashboard. Netlify will also create a preview deploy for all PR's made against `master`.

## Code

The project follows a fairly standard nuxt.js structure:

- `_content`: various json and markdown files for serving via `@nuxt/content`
- `app`: standard nuxt app directory
  - `assets`: currently just app icons which are mostly unused (TODO: use these icons?)
  - `components`: All the components!
  - `layouts`: page layouts (currenly only have a default)
  - `pages`: pages laid out via automatic page routing
  - `plugins`: Any third-party tooling that requires configuration on app load, as well as services used throughout the app (i.e. axios, notifications)
  - `static`: currently just the favicon
  - `stories`: Storybook stories
  - `styles`: PostCSS modules
- `build`: various config and build-related scripts
- `functions`: functions run by Netlify Functions

### CSS

This project utilizes PostCSS and Tailwind to handle styling. Specifically, some important PostCSS plugins are used:

- [**Import:**](https://github.com/postcss/postcss-import) Allowing the `@import` rule as per the [CSS Import](https://developer.mozilla.org/en-US/docs/Web/CSS/@import) spec, with some additional niceties.
- [**Nesting:**](https://github.com/jonathantneal/postcss-nesting) Allows nesting as per the [CSS Nesting](https://drafts.csswg.org/css-nesting-1/) spec.

## Important Tools and Libraries

### Ngrok

This app is built to be able to serve the development site via ngrok, which can be useful for sharing in-progress work with others. To keep things secure, be sure to configure the `NGROK_*` variables in `.env`.

### Storybook

TODO: it's configured but not well-utilized yet

### Netlify CLI

The netlify CLI can be used to test the production environment locally.

To install, run `npm install -g netlify-cli`.

To run, use `npm run dev:netlify`, to make the app accessible at `https://localhost:8888`. This will also make all functions available for use and testing locally.

### Sentry

TODO

### Analytics

TODO

### Rennovate

TODO
