module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    'xo-space/esnext', // https://github.com/xojs/eslint-config-xo-space
    'xo-vue/spaces', // https://github.com/ChocPanda/eslint-config-xo-vue
    'plugin:unicorn/recommended', // https://github.com/sindresorhus/eslint-plugin-unicorn
    'plugin:nuxt/recommended', // https://github.com/nuxt/eslint-plugin-nuxt
    'plugin:import/errors', // https://github.com/benmosher/eslint-plugin-import
  ],
  plugins: [],
  // Add your custom rules here
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
    semi: ['error', 'never'],
    'operator-linebreak': ['error', 'before'],
    'object-curly-spacing': ['error', 'always'],
    'space-before-function-paren': ['error', 'always'],
    'capitalized-comments': ['warn'],
    'vue/match-component-file-name': ['error', {}],
    'vue/object-curly-spacing': ['error', 'always'],
    'vue/no-boolean-default': ['error', 'default-false'],
    'vue/v-on-function-call': ['error', 'never'],
    'import/no-unresolved': 0,
    'unicorn/prevent-abbreviations': ['warn', {
      whitelist: {
        args: true,
      },
    }],
  },
}
