// Largely adapted from https://content.nuxtjs.org/integrations/#nuxtjsfeed

const hostname = process.NODE_ENV === 'production' ? 'https://rileytaylor.dev' : 'http://localhost:3000'
let posts = []

const create = async feed => {
  feed.options = {
    title: 'rileytaylor.dev - Blog',
    description: 'A development blog by Riley Taylor',
    link: `${hostname}/feed.xml`,
    language: 'en',
    favicon: 'https://rileytaylor.dev/favicon.ico',
    copyright: 'All rights reserved 2020, Riley Taylor',
    author: {
      name: 'Riley Taylor',
      email: 'rileyhtaylor@outlook.com', // TODO: change this email!!!!
      link: 'https://rileytaylor.dev',
    },
  }

  const { $content } = require('@nuxt/content')
  if (posts === null || posts.length === 0) {
    posts = await $content('posts', { text: true }).fetch()
  }

  for (const post of posts) {
    const url = `${hostname}/blog/${post.slug}`
    const content = post.text

    const feedItem = {
      title: post.title,
      id: url,
      link: url,
      date: new Date(post.createdAt),
      description: post.summary,
      content,
      image: post.image,
      author: [
        {
          name: 'Riley Taylor',
          email: 'rileyhtaylor@outlook.com', // TODO: change this email!!!!
          link: 'https://rileytaylor.dev',
        },
      ],
    }
    feed.addItem(feedItem)
  }

  return feed
}

export {
  create,
}
