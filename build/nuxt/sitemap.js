const createSitemapRoutes = async () => {
  const routes = []
  const { $content } = require('@nuxt/content')
  const posts = await $content('posts').fetch()

  for (const post of posts) {
    routes.push(`blog/${post.slug}`)
  }

  return routes
}

export {
  createSitemapRoutes,
}
