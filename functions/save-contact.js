const axios = require('axios')
const Sentry = require('@sentry/serverless')

Sentry.AWSLambda.init({
  dsn: process.env.SENTRY_DSN,
  environment: 'function',
})

const setupSendgridApi = () => axios.create({
  baseURL: 'https://api.sendgrid.com/v3/',
  timeout: 1000,
  headers: {
    authorization: `Bearer ${process.env.SENDGRID_API_KEY}`,
    'content-type': 'application/json',
  },
})

/**
 * Returns a parsed json body and REST method
 * @param {object} event the netlify http request
 * @returns {object} { data: object, method: string }
 */
const processRestCall = event => {
  return {
    data: JSON.parse(event.body),
    method: event.httpMethod,
  }
}

/**
 * Ensure sentry gets error report and a consistent error response
 * @param {object} error the error callback
 * @returns {object} error response
 */
const handleError = error => {
  console.error('Error: Response Code ->', error.response.status, error.response.statusText)
  console.error('Error: Response Data ->', error.response.data.errors)
  console.error('Error: Request Config ->', error.response.config)

  Sentry.captureException(error.response)

  return {
    statusCode: 500,
    body: JSON.stringify({
      response: `${error.response.status} ${error.response.statusText}`,
      data: error.response.data.errors,
    }),
  }
}

/**
 * Save contact info to sendgrid marketing contacts
 * @param {Axios} api - a reference to axios config
 * @param {object} params
 * @param {string} params.email - the contact email to save
 * @returns {object} an http response
 */
const saveContactInfo = async (api, { email }) => {
  if (!email) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Email is required',
        data: {
          email: email || 'undefined',
        },
      }),
    }
  }

  const body = JSON.stringify({
    contacts: [
      {
        email,
      },
    ],
  })

  try {
    const response = await api.put('/marketing/contacts', body)

    return {
      statusCode: 200,
      body: JSON.stringify({
        response: `${response.status} ${response.statusText}`,
        data: response.data,
      }),
    }
  } catch (error) {
    return handleError(error)
  }
}

/**
 * Handle requests to `/.netlify/functions/save-contact`
 * @param {object} event
 * @param {string} event.httpMethod
 * @param {object} event.headers
 * @param {object} event.queryStringParameters
 * @param {string} event.body
 * @returns {object} an http response
 */
exports.handler = Sentry.AWSLambda.wrapHandler(async event => {
  try {
    const api = setupSendgridApi()

    const { data, method } = processRestCall(event)

    if (method === 'PUT') {
      return await saveContactInfo(api, data)
    }

    return {
      statusCode: 501,
      body: JSON.stringify({
        data: `Received ${method}, which is not supported`,
      }),
    }
  } catch (error) {
    return handleError(error)
  }
})
