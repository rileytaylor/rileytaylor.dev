const Sendgrid = require('@sendgrid/mail')
const Sentry = require('@sentry/serverless')

Sentry.AWSLambda.init({
  dsn: process.env.SENTRY_DSN,
  environment: 'function',
})

const setupSendgrid = () => Sendgrid.setApiKey(process.env.SENDGRID_API_KEY)

/**
 * Returns a parsed json body and REST method
 * @param {object} event the netlify http request
 * @returns {object} { data: object, method: string }
 */
const processRestCall = event => {
  return {
    data: JSON.parse(event.body),
    method: event.httpMethod,
  }
}

/**
 * Ensure sentry gets error report and a consistent error response
 * @param {object} error the error callback
 * @returns {object} error response
 */
const handleError = error => {
  Sentry.captureException(error)

  return {
    statusCode: 500,
    body: error,
  }
}

/**
 * Send the contact's message to myself via sendgrid
 * @param {object} params
 * @param {string} params.email the contacts email
 * @param {string} params.name the contacts name (if provided)
 * @param {string} params.message the contacts message
 * @returns {object} an http response
 */
const sendContactMeEmail = async ({ email, name, message }) => {
  if (!email || !message) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'Email and Message are required',
        data: {
          email: email || 'undefined',
          message: message || 'undefined',
        },
      }),
    }
  }

  const mail = {
    to: 'riley@rileytaylor.dev',
    from: {
      email: 'site@rileytaylor.dev',
      name: 'Personal Site',
    },
    replyTo: {
      email,
      name,
    },
    subject: `Hello from ${name ? name : email}`,
    text: message,
  }

  try {
    await Sendgrid.send(mail)
    return {
      statusCode: 202,
    }
  } catch (error) {
    return handleError(error)
  }
}

/**
 * Handle requests to `/.netlify/functions/contact-me`
 * @param {object} event
 * @param {string} event.httpMethod
 * @param {object} event.headers
 * @param {object} event.queryStringParameters
 * @param {string} event.body
 * @returns {object} an http response
 */
exports.handler = Sentry.AWSLambda.wrapHandler(async event => {
  try {
    setupSendgrid()

    const { data, method } = processRestCall(event)

    if (method === 'POST') {
      return await sendContactMeEmail(data)
    }

    return {
      statusCode: 501,
      body: `Received ${method}, which is not supported`,
    }
  } catch (error) {
    return handleError(error)
  }
})
