/*
 * TailwindCSS Configuration File
 *
 * Docs: https://tailwindcss.com/docs/configuration
 * Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

// TODO: Disable any unused things, properly set up purgecss

module.exports = {
  // mode: 'jit',
  theme: {
    colors: {
      white: colors.white,
      black: colors.black,
      transparent: 'transparent',
      current: 'currentColor',
      primary: {
        50: colors.amber[50],
        100: colors.amber[100],
        200: colors.amber[200],
        300: colors.amber[300],
        400: colors.amber[400],
        500: colors.amber[500],
        600: colors.amber[600],
        700: colors.amber[700],
        800: colors.amber[800],
        900: colors.amber[900],
      },
      neutral: {
        50: colors.trueGray[50],
        100: colors.trueGray[100],
        200: colors.trueGray[200],
        300: colors.trueGray[300],
        400: colors.trueGray[400],
        500: colors.trueGray[500],
        600: colors.trueGray[600],
        700: colors.trueGray[700],
        800: colors.trueGray[800],
        900: colors.trueGray[900],
      },
      // TODO: Come up with more theme colors
      accent: {
        50: colors.indigo[50],
        100: colors.indigo[100],
        200: colors.indigo[200],
        300: colors.indigo[300],
        400: colors.indigo[400],
        500: colors.indigo[500],
        600: colors.indigo[600],
        700: colors.indigo[700],
        800: colors.indigo[800],
        900: colors.indigo[900],
      },
      success: {
        400: colors.green[400],
        500: colors.green[500],
        600: colors.green[600],
      },
      error: {
        400: colors.red[400],
        500: colors.red[500],
        600: colors.red[600],
      },
      warning: {
        400: colors.orange[400],
        500: colors.orange[500],
        600: colors.orange[600],
      },
    },
    fontFamily: {
      display: ['Josefin Slab', ...defaultTheme.fontFamily.serif],
      header: ['Josefin Sans', ...defaultTheme.fontFamily.sans],
      body: ['Mulish', ...defaultTheme.fontFamily.sans],
    },
    // TODO: Disable some of these if unused
    screens: {
      phoneWide: '36em', // 576px
      tablet: '48em', // 768px
      laptop: '62em', // 992px
      desktop: '75em', // 1200px
      ultrawide: '87.5em', // 1400px
    },
    extend: {
      fontWeight: {
        bolditalic: '700i',
      },
      transitionProperty: {
        size: 'height, width, margin, padding',
      },
      // Typography plugin config for posts
      typography: {
        DEFAULT: {
          css: {
            color: colors.trueGray[100],
            '[class~="lead"]': {
              color: colors.trueGray[200],
            },
            a: {
              color: colors.amber[600],
              '&:hover': {
                color: colors.amber[700],
              },
            },
            strong: {
              color: colors.trueGray[100],
            },
            hr: {
              borderColor: colors.amber[600],
              borderTopWidth: 2,
            },
            blockquote: {
              color: colors.trueGray[200],
              borderLeftColor: colors.amber[600],
            },
            h1: {
              color: colors.trueGray[100],
            },
            h2: {
              color: colors.trueGray[100],
            },
            h3: {
              color: colors.trueGray[100],
            },
            h4: {
              color: colors.trueGray[100],
            },
            code: {
              color: colors.amber[400],
            },
            thead: {
              color: colors.trueGray[300],
              borderBottomColor: colors.trueGray[500],
              borderBottomWidth: 2,
            },
            'tbody tr': {
              borderBottomColor: colors.trueGray[700],
            },
            'ol > li::before': {
              color: colors.trueGray[400],
            },
            'ul > li::before': {
              color: colors.trueGray[400],
            },
          },
        },
      },
    },
  },
  variants: {
    extend: {
      ringWidth: ['hover', 'focus'],
      ringColor: ['hover', 'focus'],
      ringOpacity: ['hover', 'focus'],
    },
  },
  corePlugins: {
    preflight: false,
  },
  plugins: [
    require('@tailwindcss/forms')({
      strategy: 'class',
    }),
    require('@tailwindcss/typography'), // Tailwind Typography: https://github.com/tailwindlabs/tailwindcss-typography
  ],
  // TODO: purge the things
  // purge: [
  //   'app/components/**/*.{vue,js}',
  //   'app/layouts/**/*.vue',
  //   'app/pages/**/*.vue',
  //   'app/plugins/**/*.js',
  //   'nuxt.config.{js,ts}',
  // ],
}
