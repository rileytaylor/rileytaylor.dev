import path from 'path'
import SentryWebpackPlugin from '@sentry/webpack-plugin'
import { createSitemapRoutes } from './build/nuxt/sitemap'
import { create } from './build/nuxt/feed'

export default {
  target: 'static',
  srcDir: 'app/',

  // Auto-import components (necessary for components used in @nuxt/content posts)
  components: true,

  // Environment variables
  env: {
    isDev: process.env.NODE_ENV !== 'production',
    testPosts: process.env.TEST_POSTS || false,
  },
  publicRuntimeConfig: {
    sentryDsn: process.env.SENTRY_DSN || '',
    version: process.env.npm_package_version,
    baseURL: process.env.BROWSER_BASE_URL,
  },

  // Meta config
  head: {
    titleTemplate: title => {
      // If undefined/blank it's the index page or we don't have a sub-title
      return title ? `${title} - Riley Taylor` : 'Riley Taylor'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Software Developer',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Customize the progress-bar color
  loading: {
    color: '#ed8936',
    hieght: '3px',
  },

  // Global CSS imports
  css: [
    '~/styles/main.css',
  ],

  // Plugins
  plugins: [
    '~/plugins/fontawesome',
    '~/plugins/tailwind',
    '~/plugins/sentry',
    '~/plugins/axios',
  ],

  // Dev Modules
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    // Doc: https://ngrok.nuxtjs.org
    // '@nuxtjs/ngrok',
  ],

  // Modules
  modules: [
    // Content: https://content.nuxtjs.org/
    '@nuxt/content',
    // Feed: https://github.com/nuxt-community/feed-module
    '@nuxtjs/feed',
    // Sitemap: https://github.com/nuxt-community/sitemap-module
    '@nuxtjs/sitemap',
  ],

  // Webpack Build config
  build: {
    /*
     ** You can extend webpack config here
     */
    extend (config, { isClient, isDev }) {
      if (isClient) {
        // No fs in static builds, tailwind tries to use it for some reason
        config.node = {
          fs: 'empty',
        }

        config.plugins.push(
          new SentryWebpackPlugin({
            authToken: process.env.SENTRY_CLI_AUTH_TOKEN,
            org: 'riley-taylor',
            project: 'rileytaylordotdev',
            include: path.join(__dirname, '.nuxt/dist'),
            ignore: ['node_modules', '_content'],
            dryRun: !isDev, // Don't upload unless this is a production build
          }),
        )
      }
    },
    babel: {
      babelrc: true,
    },
    postcss: {
      plugins: {
        'postcss-preset-env': false,
        'postcss-import': {
          root: path.join(__dirname, 'node_modules'),
          path: [
            path.join(__dirname, 'app', 'styles'),
          ],
        },
        'postcss-url': {},
        'postcss-custom-media': {
          importFrom: [
            path.join(__dirname, 'app', 'styles', 'config', 'custom-media.css'),
          ],
        },
        // Ensure tailwind config executes earlier and with nesting in mind, this doesn't mess up @nuxtjs/tailwind
        'tailwindcss/nesting': require('postcss-nesting'),
        tailwindcss: path.join(__dirname, 'tailwind.config.js'),
        autoprefixer: {
          grid: true,
        },
        cssnano: {
          preset: 'default',
          discardComments: { removeAll: true },
        },
        'postcss-reporter': {},
      },
    },
    terser: {
      sourceMap: true,
    },
  },

  // @nuxt/content config
  content: {
    dir: path.join(__dirname, '_content'),
    liveEdit: false,
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-dracula.css',
      },
      tocDepth: 4,
      remarkPlugins: () => [
        'remark-squeeze-paragraphs',
        'remark-slug',
        // https://github.com/remarkjs/remark-autolink-headings
        ['remark-autolink-headings', {
          behavior: 'append',
          content: {
            type: 'element',
            tagName: 'i',
            properties: {
              className: ['header-anchor', 'far', 'fa-hashtag'],
            },
          },
        }],
        'remark-external-links',
        'remark-images', // https://github.com/remarkjs/remark-images,
        'remark-oembed', // https://github.com/sergioramos/remark-oembed
        'remark-gfm', // https://github.com/remarkjs/remark-gfm
        'remark-emoji', // https://github.com/rhysd/remark-emoji
        'remark-footnotes', // https://github.com/remarkjs/remark-footnotes
      ],
      rehypePlugins: () => [
        'rehype-sort-attribute-values',
        'rehype-sort-attributes',
        'rehype-raw',
      ],
    },
  },

  // @nuxt/storybook: https://storybook.nuxtjs.org/
  storybook: {
    addons: [
      '@storybook/addon-actions',
      '@storybook/addon-controls',
      '@storybook/addon-notes',
    ],
    stories: [
      '~/stories/**/*.stories.js',
    ],
    webpackFinal (config) {
      return config
    },
  },

  // Sitemap config
  sitemap: {
    hostname: 'https://riletaylor.dev',
    gzip: true,
    routes: createSitemapRoutes,
  },

  // @nuxtjs/feed config
  feed: [
    {
      path: '/feed.xml',
      create,
      cacheTime: 1000 * 60 * 15,
      type: 'rss2',
    },
  ],

  // Tailwindcss nuxt module config
  tailwindcss: {
    configPath: '../tailwind.config.js',
    cssPath: '~/styles/tailwind.css',
    // jit: true,
  },

  // Ngrok config
  ngrok: {
    authtoken: process.env.NGROK_AUTHTOKEN,
    auth: process.env.NGROK_AUTH,
  },
}
