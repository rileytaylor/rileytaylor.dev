import Vue from 'vue'
import { library, config, dom } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faAddressCard,
  faArrowLeft,
  faArrowRight,
  faBars,
  faBookReader,
  faCertificate,
  faClarinet,
  faCommentAltLines,
  faDiceD20,
  faEnvelope,
  faFileCode,
  faFileUser,
  faFlask,
  faGameBoard,
  faHashtag,
  faHatWizard,
  faMagic,
  faPaperPlane,
  faRss,
  faServer,
  faTimes,
  faToolbox,
  faLevelDownAlt,
} from '@rileytaylor/pro-regular-svg-icons'
import {
  faBrowser,
  faChalkboardTeacher,
  faHatWizard as faHatWizardDuotone,
  faTrafficCone,
  faExclamationSquare,
  faExclamationTriangle,
  faCheckSquare,
  faCommentAltSmile,
  faSpinnerThird,
} from '@rileytaylor/pro-duotone-svg-icons'
import {
  faCss3Alt,
  faDocker,
  faEmber,
  faGithub,
  faGitlab,
  faHtml5,
  faJsSquare,
  faLinkedin,
  faLinux,
  faPython,
  faSass,
  faTwitter,
  faVuejs,
} from '@rileytaylor/pro-brands-svg-icons'

// This is important, we are going to let Nuxt.js worry about the css
config.autoAddCss = false

// Add icons to the library
library.add(
  // Regular
  faAddressCard,
  faArrowLeft,
  faArrowRight,
  faBars,
  faBookReader,
  faCertificate,
  faClarinet,
  faCommentAltLines,
  faDiceD20,
  faEnvelope,
  faFileCode,
  faFileUser,
  faFlask,
  faGameBoard,
  faHashtag,
  faHatWizard,
  faMagic,
  faPaperPlane,
  faRss,
  faServer,
  faTimes,
  faToolbox,
  faLevelDownAlt,
  // Duotone
  faBrowser,
  faChalkboardTeacher,
  faHatWizardDuotone,
  faTrafficCone,
  faExclamationSquare,
  faExclamationTriangle,
  faCheckSquare,
  faCommentAltSmile,
  faSpinnerThird,
  // Brands
  faCss3Alt,
  faDocker,
  faEmber,
  faGithub,
  faGitlab,
  faHtml5,
  faJsSquare,
  faLinkedin,
  faLinux,
  faPython,
  faSass,
  faTwitter,
  faVuejs,
)

Vue.component('FontAwesomeIcon', FontAwesomeIcon)

// Watch for i tags using font awesome without the vue component
// https://github.com/FortAwesome/vue-fontawesome#processing-i-tags-into-svg-using-font-awesome
dom.watch()
