import Vue from 'vue'
import { v4 as uuidV4 } from 'uuid'

const EVENT_BUS = new Vue()

class NotifyGroupRegistrationError extends Error {
  constructor (name) {
    super()
    this.message = `Group "${name}" already exists`
    this.name = 'NotifyGroupRegistrationError'
  }
}

class Notify {
  constructor () {
    this.groups = []
    this.eventBus = EVENT_BUS
  }

  registerGroup (name) {
    if (this.groups.includes(name)) {
      throw new NotifyGroupRegistrationError(name)
    }

    this.groups.push(name)
  }

  create (group, data) {
    const id = uuidV4()
    data.id = id
    EVENT_BUS.$emit('notify:create', {
      group: group || 'default',
      data,
    })
    return id
  }

  remove (id) {
    EVENT_BUS.$emit('notify:remove', id)
  }

  empty (group) {
    EVENT_BUS.$emit('notify:empty', group)
  }
}

const NotificationsPlugin = inject => {
  inject('notify', new Notify())
}

export default NotificationsPlugin
