import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from '@@/tailwind.config.js'

const fullConfig = resolveConfig(tailwindConfig)

export default (context, inject) => {
  inject('tailwind', fullConfig)
}
