import Vue from 'vue'
import * as Sentry from '@sentry/vue'
import {
  CaptureConsole as CaptureConsoleIntegration,
  Dedupe as DedupeIntegration,
} from '@sentry/integrations'
import { Integrations as TracingIntegrations } from '@sentry/tracing'

const SentryPlugin = (context, inject) => {
  const config = {
    Vue,
    dsn: context.$config.sentryDsn,
    environment: context.isDev ? 'local' : 'production',
    attachProps: true,
    logErrors: true,
    integrations: [
      new TracingIntegrations.BrowserTracing(),
      new CaptureConsoleIntegration({
        levels: ['error'],
      }),
      new DedupeIntegration(),
    ],
    // eslint-disable-next-line unicorn/no-zero-fractions
    tracesSampleRate: 1.0,
    tracing: true,
    tracingOptions: {
      hooks: ['mount', 'update'],
      timeout: 2000,
      trackComponents: true,
    },
  }

  Sentry.init(config)

  // Access sentry as Vue.$sentry
  inject('sentry', Sentry)
}

export default SentryPlugin
