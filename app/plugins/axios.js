import axios from 'axios'

const AxiosPlugin = (context, inject) => {
  const http = axios.create({
    baseURL: context.$config.baseURL || 'http://localhost:8888',
    // Don't reject the promise on api response failures
    validateStatus: () => true,
  })

  // Access axios as Vue.$http
  inject('http', http)
}

export default AxiosPlugin
