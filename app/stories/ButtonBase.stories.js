import ButtonBase from '../components/ButtonBase'

// This default export determines where your story goes in the story list
export default {
  title: 'Button',
  component: ButtonBase,
}

const Template = args => ({
  components: { ButtonBase },
  props: { args },
  template: '<ButtonBase v-bind="$props">Button</ButtonBase>',
})

export const Default = Template.bind({})
Default.args = {}
