---
title: print('Hello, world!')
subtitle: Gotta to start somewhere
summary: I've written this many times over many attempts to start a blog. Maybe this one will stick?
publishDate: '2021-05-01T00:00:00'
image: https://rht.sfo2.cdn.digitaloceanspaces.com/blog/images/2021-hello-world.jpg
imageAlt: 'Image of a starry deep-space nebula, meant to invoke feelings of space nostalgia and grand ambition'
tags:
 - about
---

I don't know if blogs need a "hello world" post per say (and it's not like there are rules anyway) but I always appreciate them when they do exist. If this post is entirely for me to establish my goals, fine. But if someone reads this and finds inspiration or a deeper interest in my content from it, even better.

I started learning how to code through a combination of blogs and online video tutorials. When I reach a problem, my first attempt to find a solution (after debugging as much as I can handle) has always been to search the internet (surprise?). This usually leads to one of three places: Stack Overflow, official documentation for whatever I'm using, or someone's blog. I'm sure that I am not alone when I say that many (most?) solutions to some of the problems I face in my code have been solved purely because a programmer like myself had decided to write about how they figured it out. If I can contribute to at least one other person quickly solving something I spent a ridiculous amount of time-solving myself, then I consider this blog a success.

This will likely also be a place for informational or philisophical posts, including some things that may not be explicitely software development.

![Philosophy](https://media.giphy.com/media/1aAeJ6FOp1JnO/source.gif)

I want to view this as more of "my blog" than "my development blog" While I understand that getting some random posts in your RSS feed that aren't expected might be annoying, it's also easy to just mark those as read and move in if there is no interest. It seems a lot of the people I've kept up with over the years-- via blogs, youtube, twitch, etc-- have all eventually made a transition to their content being broader. Maybe there's a focus on music, or computers, or product reviews, but that shouldn't restrict things. In youtube, this often results in channels changing their names to the creators name. Or having a few "subchannels".

I'm not interested in making multiple sites (lets not talk about how many hours this one took, and lets not even _think_ about the several abandoned attempts previous to this one). This is a living, breathing site. As such, I'll adapt it as I go. I don't know how much this blog will grow or be read, but perhaps I'll set up some tag-filtered RSS feeds if it becomes popular enough that it matters. Otherwise, I hope you enjoy some or all the things I write about!

## Great, so what will you write about?

My first language was C\#. While I really enjoyed the language, I don't really do anything in the Microsoft realm anymore. Maybe I got burned by the whole ~Windows 8~ ~Windows 8.1~ UWP thing? Yeah. That was a nightmare.

My work has primarily been in web development for the past seven years. My favorite stack so far has been [Vue.js](https://vuejs.org) and [Django](https://djangoproject.com), both of which I've adoped for personal projects. Vue.js is awesome, well-documented, and the community really keeps things going. I enjoy most aspects of application development from front-end to back-end, UI/UX to DevOps, but front-end is definitely my muse so I'll likely write about it the most.

My current job (as of this post) utilizes [Ember.js](https://emberjs.com). I spent some time in Ember early on in my web development career (circa 2016), so it's been interesting diving back into it with Ember Octane Edition.

I also run a home server, DIY woodworking projects, lots of house rennovation things, and am a music lover. More on the music thing, coming right up!

## Who are you?

I grew up mostly in Phoenix, Arizona. I grauduated in 2018 with Bachelors of Music Performance (Clarinet) from the University of Arizona (_"BEAR DOWN! Go Cats!"_) after moving to Tucson, where I currently live.

![Clarinet](https://media.giphy.com/media/hAiTTBy2R8DiU/source.gif)

My degree is in Clarinet performance, though I played Piano for most of my childhood and teenage years and also play the saxophone. 

I've been into computers as long as I can remember, helping my dad build our various family computers since I was 3. I remember helping him build a Windows 98 machine that was the start of my love of computing. The first game I remember playing was [Star Wars Rebel Assault II](https://en.wikipedia.org/wiki/Star_Wars:_Rebel_Assault_II:_The_Hidden_Empire), which is still one of my favorite games and I keep it available on DOSBox for play on occasion. Real talk: I love star wars games, especially the golden era before 2008. We won't talk about how many times I've replayed [Knights of the Old Republic II: The Sith Lords](https://starwars.fandom.com/wiki/Star_Wars:_Knights_of_the_Old_Republic_II:_The_Sith_Lords).

I built my first computer in middle school using Ubuntu 8.10. Remember when you had to get the cd's mailed to you? Yeah. That. I started becoming interested in code around that time, leaning some Visual Basic and doing a bit of HTML4, but I didn't really get into programming until the last year of high school when I really hunkered down and learned C\#.

## What's the blog made with?

I'll go into detail, but the tl;dr is [**JAMStack**](https://jamstack.org/) hosted on [Netlify](https://netlify.com), which is a really slick platform.

This site is built with [Vue.js](https://vuejs.org) and [Nuxt.js](https://nuxtjs.org). Nuxt.js provides a nice pattern to build an app on top of, sorta like a MVC platform. For this site I wanted something easier to maintain, and the Nuxt.js community builds a lot of plugins, compatibility layers, and modules which make using packages and implementing features pretty simple. Normally I think a more DIY approach with Vue CLI (maybe Vite in the near future) is the way to go for things. While the Nuxt.js ecosystem has made for a lot of simplicity, it's also made for a lot of headaches. Especially around sensible defaults and config overrides. Nuxt plugins will often make decisions for you around the library it wraps. Unfortunately, most of the time you won't be able to override those decisions. It's rather frustrating. Many, many times I would import a nuxt plugin and spend a few hours working with it, only to just write my own thin wrapper plugin or use the library directly. The Vite community is fairly quickly building a lot of plugins that mimic some of the best Nuxt features, so perhaps that will be a transition this site eventually makes.

The UI is built with [Tailwind](https://tailwindcss.com). Tailwind is a refreshing escape, and it's far more modular and less opinionated. I've come to really enjoy how simple it is to build an individualized design language around.

You may be wondering at this point what the backend or CMS I'm using is. The answer is... none! All my posts are stored as markdown within the git repository and statically generated by Nuxt before deploy. This is made simple through [Nuxt Content](https://content.nuxtjs.org/), which is way better than the custom system I was trying to build beforehand. To be honest, Nuxt Content is a big part of the reason this site has even made it to publishing, and (at least for now) allows me to forgive all my other Nuxt-related gripes.

Funny thing about personal web sites... the simpler they are, the better. Yes, one should probably build and show off their skills if they are a web developer, but at the same time if it's not a money making site, it can't take too much time. I've started down this path towards a personal web site a few times now, and each time I never made it to the finish line because it became far too complicated. While building a design system from scratch may seem like a good idea, it became way too much overhead to refine in my spare time for something that needed to be production ready and stable.

What I've really enjoyed with Tailwind is how easy it is to make components with, and how it takes away a lot of decision paralysis without sacrificing flexibility. I did choose to make most of the components used in the site myself rather than use third-party ones, but that's with the intent of using this site as an example of my work.

Real talk though: a simple custom thing is easier to maintain than an imported library. This is especially true ~if~ when breaking changes are introduced, or their components become more complex to support bigger projects that need that complexity. No matter how highly I think of this site, the reality is that it does not need the same flexibility that a SaaS app might. Sure, projects like Vuetify have massive followings (and for good reason!) but the complexity required to _know_ that framework is too much for most projects.

If you are thinking about your own personal site, I'd keep that in mind: what's the most you are willing to do to keep it up to date? There's nothing wrong with just hosting a Ghost site, or using a Gatsby template with minimal changes. Consider using third-party libraries to fill gaps, but when that library is massive, consider whether you ~want~ need all the overhead they provide.

## Get on with it!

![Get on with it](https://media.giphy.com/media/oGAM2NfiX50ac/source.gif)

Ok, ok. Time to write some real blog posts. Thanks for reading :smile:
